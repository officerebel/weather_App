# Weather app

A blog like app with backend and frontend

## Getting Started

    code
$ git clone https://gitlab.com/officerebel/weather_App.git

$ cd weather_App/weather_App

### Open two terminal windows
one for frontend and one for backend

### the Backend

```
Make a virtual environment
$virtualenv weather_venv
$source weather_venv/bin/activate

Install the requirements
$ pip3 install -r requirements.txt 

```

### set the secret key

```
### Activate the server
```
$ python3 manage.py runserver
```

```







## Built With

* [Django](https://www.djangoproject.com) - The web framework used


## Authors

* **Tim Vogt** - *Initial work* - [Officerebel](https://gitlab.com/officerebel)



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

